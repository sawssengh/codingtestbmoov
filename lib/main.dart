import 'package:coding_test_bmoov/screens/login_screen.dart';
import 'package:coding_test_bmoov/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Style.red),
          useMaterial3: true,
          scaffoldBackgroundColor: Style.bgColor,
        ),
        home: const LoginScreen(),
      ),
    );
  }
}
