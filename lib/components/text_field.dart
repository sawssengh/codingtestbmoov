import 'package:coding_test_bmoov/utils/colors.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget getTextField(
    {required String hint,
    required TextEditingController controller,
    keyboardtype,
    List<TextInputFormatter>? inputFormatter,
    String? Function(String?)? validator,
    IconData? preIcon,
    Color? preIconColor,
    IconButton? suffIconButton,
    IconData? suffIcon,
    Color? suffIconColor,
    Function(String)? onChange,
    Function()? onPress,
    Function(String)? onSubmitted,
    ot}) {
  return TextFormField(
    obscureText: ot,
    keyboardType: keyboardtype,
    onChanged: (value) {
      if (onChange != null) {
        onChange(value);
      }
    },
    onFieldSubmitted: (value) {
      if (onSubmitted != null) {
        // Check if onSubmitted is provided
        onSubmitted(value);
      }
    },
    inputFormatters: inputFormatter,
    controller: controller,
    validator: validator,
    decoration: InputDecoration(
    
        hintText: hint,
        hintStyle: TextStyle(
            fontSize: 15.sp,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            color: Style.grey),
        prefixIcon: Icon(preIcon, color: preIconColor),
        suffixIcon: IconButton(
          icon: Icon(suffIcon),
          onPressed: onPress,
          color: suffIconColor,
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.5),
            borderSide: const BorderSide(color: Colors.transparent)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.5),
            borderSide: const BorderSide(color: Colors.transparent)),
        filled: true,
        fillColor: Style.lightGrey),
  );
}
