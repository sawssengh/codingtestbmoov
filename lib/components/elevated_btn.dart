import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget getElevatedButton(
    {required String title,
    required fontsize,
    IconData? icon,
    required bgColor,
    required fgColor,
    Function()? onPress}) {
  return ElevatedButton(
    style: ButtonStyle(
        minimumSize: MaterialStateProperty.all(Size(100.w, 80.h)),
        backgroundColor: MaterialStateProperty.all(bgColor),
        foregroundColor: MaterialStateProperty.all(fgColor),
        shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)))),
    onPressed: onPress,
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: fontsize,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          width: 80.w,
        ),
        Icon(icon, color: Colors.white),
      ],
    ),
  );
}
