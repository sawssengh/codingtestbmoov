import 'package:coding_test_bmoov/screens/jokes_screen.dart';

import 'package:coding_test_bmoov/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailJoke extends StatefulWidget {
  final int id;
  final String setup;
  const DetailJoke({required this.id, required this.setup, super.key});

  @override
  State<DetailJoke> createState() => _DetailJokeState();
}

class _DetailJokeState extends State<DetailJoke> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Style.red,
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Style.red,
        centerTitle: true,
        title:
            Text('${widget.id}', style: const TextStyle(color: Colors.white)),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => const JokesScreen()),
            );
          },
        ),
      ),
      body: Center(
        child: Container(
          height: 250,
          width: 250,
          color: Style.darkYellow,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                widget.setup,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 18.sp),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
