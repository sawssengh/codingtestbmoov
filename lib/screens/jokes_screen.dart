import 'dart:convert';

import 'package:coding_test_bmoov/models/joke.dart';
import 'package:coding_test_bmoov/screens/details_joke.dart';
import 'package:coding_test_bmoov/utils/colors.dart';
import 'package:coding_test_bmoov/utils/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;

class JokesScreen extends StatefulWidget {
  const JokesScreen({Key? key}) : super(key: key);

  @override
  State<JokesScreen> createState() => _JokesScreenState();
}

class _JokesScreenState extends State<JokesScreen> {
  //var
  List<Joke> jokes = [];

  //actions
  Future<List<Joke>> getJokes() async {
    //url
    Uri verifyUri = Uri.parse("$baseUrl/jokes/ten");

    //request
    try {
      final response = await http.get(verifyUri);

      if (response.statusCode == 200) {
        var jsonObject = jsonDecode(response.body);

        //test
        print(jsonObject);

        jsonObject.forEach((joke) => {
              jokes.add(Joke(
                  joke['id'], joke['type'], joke['setup'], joke['punchline']))
            });
        return jokes;
      } else {
        print('HTTP request failed with status: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print('HTTP request failed with error: $e');
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          systemOverlayStyle: const SystemUiOverlayStyle(

            statusBarColor: Style.red,
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Style.red,
          flexibleSpace: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  "Jokes",
                  style: TextStyle(color: Colors.white, fontSize: 20.sp),
                ),
              ),
            ],
          ),
        ),
        body: FutureBuilder<List<Joke>>(
            future: getJokes(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Joke>> snapshot) {
              if (!snapshot.hasData) {
                return const Center(child: CircularProgressIndicator());
              } else {
                List<Joke> pdata = snapshot.data!;
                print(pdata);
                return ListView.builder(
                    itemCount: pdata.length,
                    itemBuilder: (context, index) {
                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0.0),
                        ),
                        color: Style.yellow,
                        elevation: 3,
                        child: ListTile(
                          trailing: const Icon(CupertinoIcons.arrow_right,
                              color: Style.grey),
                          leading:
                              const Icon(Icons.agriculture, color: Style.grey),
                          title: Text(pdata[index].setup!,
                              softWrap: true, maxLines: 5),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => DetailJoke(
                                    id: pdata[index].id!,
                                    setup: pdata[index].setup!)));
                          },
                        ),
                      );
                    });
              }
            }));
  }
}
