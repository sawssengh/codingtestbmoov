import 'package:coding_test_bmoov/components/text_btn.dart';
import 'package:coding_test_bmoov/components/text_field.dart';
import 'package:coding_test_bmoov/screens/ForgetPassword/forget_password_A.dart';
import 'package:coding_test_bmoov/screens/jokes_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../utils/colors.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //var
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  dynamic _isObscureText;

  //controller
  TextEditingController login = TextEditingController();
  TextEditingController password = TextEditingController();

  //validator

  @override
  void initState() {
    super.initState();
    _isObscureText = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Form(
            key: _keyForm,
            child: ListView(children: [
              SizedBox(height: 200.h),
              getTextField(
                  ot: false,
                  hint: "Login",
                  controller: login,
                  onChange: (val) {},
                  validator: (val) {
                    if (val!.isEmpty) {
                      return "Login cannot be empty!";
                    }
                    return null;
                  },
                  preIcon: Icons.supervised_user_circle,
                  preIconColor: Style.darkGrey),
              SizedBox(height: 40.h),
              getTextField(
                  ot: _isObscureText,
                  hint: "Password",
                  controller: password,
                  onChange: (val) {},
                  validator: (val) {
                    if (val!.isEmpty) {
                      return "Password cannot be empty!";
                    } else {
                      if (val.length < 8) {
                        return "Password must be at least 8 characters";
                      } else if (!RegExp(r'^(?=.*[\W]).{8,}$').hasMatch(val)) {
                        return 'Password must contain a special caracter'; // Password must contain at least one special character
                      }
                    }
                    return null;
                  },
                  preIcon: Icons.lock,
                  preIconColor: Style.darkGrey,
                  suffIcon:
                      _isObscureText ? Icons.visibility : Icons.visibility_off,
                  onPress: () {
                    setState(() {
                      _isObscureText = !_isObscureText;
                    });
                  },
                  suffIconColor: Style.darkGrey),
              SizedBox(height: 25.h),
              Align(
                alignment: Alignment.topRight,
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const ForgetPasswordA()));
                  },
                  child: Text(
                    'Forgot password',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 50.h),
              getTextButton(
                width: double.infinity,
                height: 70.h,
                bgColor: Style.orange,
                fgColor: Colors.white,
                title: "Login",
                fontsize: 20.sp,
                onPressed: () {
                  if (_keyForm.currentState!.validate()) {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => const JokesScreen()));
                  }
                },
                context: context,
              ),
            ])),
      )),
    );
  }
}
