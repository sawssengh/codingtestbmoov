// ignore_for_file: file_names

import 'package:coding_test_bmoov/components/elevated_btn.dart';
import 'package:coding_test_bmoov/components/text_field.dart';
import 'package:coding_test_bmoov/screens/ForgetPassword/forget_password_B.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../utils/colors.dart';

class ForgetPasswordA extends StatefulWidget {
  const ForgetPasswordA({super.key});

  @override
  State<ForgetPasswordA> createState() => _ForgetPasswordAState();
}

class _ForgetPasswordAState extends State<ForgetPasswordA> {
  //var
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  //controller
  TextEditingController email = TextEditingController();

  //validator

  String? emailValidator(String? email) {
    final emailRegExp = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    final isEmailValid = emailRegExp.hasMatch(email!);
    if (!isEmailValid) {
      return "Please enter a valid E-mail";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Style.bgColor,
        appBar: AppBar(
            systemOverlayStyle: const SystemUiOverlayStyle(
              statusBarColor: Style.bgColor,
            ),
            centerTitle: true,
            leading: const BackButton(color: Style.orange),
            title: Text("Forgot password",
                style: TextStyle(color: Style.orange, fontSize: 15.sp))),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Form(
              key: _keyForm,
              child: Column(
                children: [
                  SizedBox(height: 80.h),
                  Text(
                      "Please enter your email and we will send you \n a code to change your password",
                      style: TextStyle(fontSize: 13.sp),
                      textAlign: TextAlign.center),
                  SizedBox(height: 50.h),
                  getTextField(
                      ot: false,
                      hint: "E-mail",
                      controller: email,
                      keyboardtype: TextInputType.emailAddress,
                      onChange: (val) {},
                      validator: emailValidator,
                      preIcon: Icons.email,
                      preIconColor: Style.darkGrey),
                  SizedBox(height: 40.h),
                  getElevatedButton(
                    title: "Continue",
                    icon: Icons.arrow_forward_ios,
                    fontsize: 20.sp,
                    bgColor: Style.orange,
                    fgColor: Colors.white,
                    onPress: () {
                      if (_keyForm.currentState!.validate()) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ForgetPasswordB(
                                  email: email.text,
                                )));
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
