// ignore_for_file: file_names

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:coding_test_bmoov/components/text_btn.dart';
import 'package:coding_test_bmoov/components/text_field.dart';
import 'package:coding_test_bmoov/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ForgetPasswordC extends StatefulWidget {
  const ForgetPasswordC({super.key});

  @override
  State<ForgetPasswordC> createState() => _ForgetPasswordCState();
}

class _ForgetPasswordCState extends State<ForgetPasswordC> {
  //var
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  dynamic _isObscureTextPassword;
  dynamic _isObscureTextConfirmPassword;

  //controller
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();

  @override
  void initState() {
    super.initState();
    _isObscureTextPassword = true;
    _isObscureTextConfirmPassword = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Style.bgColor,
        appBar: AppBar(
            systemOverlayStyle: const SystemUiOverlayStyle(
              statusBarColor: Style.bgColor,
            ),
            centerTitle: true,
            leading: const BackButton(color: Style.orange),
            title: Text("Forgot password",
                style: TextStyle(color: Style.orange, fontSize: 15.sp))),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Form(
              key: _keyForm,
              child: ListView(
                children: [
                  SizedBox(height: 80.h),
                  Text("Please enter your new password",
                      style: TextStyle(fontSize: 13.sp),
                      textAlign: TextAlign.center),
                  SizedBox(height: 50.h),
                  getTextField(
                      ot: _isObscureTextPassword,
                      hint: "Password",
                      controller: password,
                      onChange: (val) {},
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Password cannot be empty!";
                        } else {
                          if (val.length < 8) {
                            return "Password must be at least 8 characters";
                          } else if (!RegExp(r'^(?=.*[\W]).{8,}$')
                              .hasMatch(val)) {
                            return 'Password must contain a special caracter'; // Password must contain at least one special character
                          }
                        }
                        return null;
                      },
                      preIcon: Icons.lock,
                      preIconColor: Style.darkGrey,
                      suffIcon: _isObscureTextPassword
                          ? Icons.visibility
                          : Icons.visibility_off,
                      onPress: () {
                        setState(() {
                          _isObscureTextPassword = !_isObscureTextPassword;
                        });
                      },
                      suffIconColor: Style.darkGrey),
                  SizedBox(height: 10.h),
                  getTextField(
                      ot: _isObscureTextConfirmPassword,
                      hint: "Confirm Password",
                      controller: confirmPassword,
                      onChange: (val) {},
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Confirm Password cannot be empty!";
                        } else {
                          if (val.length < 8) {
                            return "Confirm Password must be at least 8 characters";
                          } else if (!RegExp(r'^(?=.*[\W]).{8,}$')
                              .hasMatch(val)) {
                            return 'Confirm Password must contain a special caracter'; // Password must contain at least one special character
                          } else if (val != password.text) {
                            AwesomeDialog(
                              context: context,
                              dialogType: DialogType.error,
                              animType: AnimType.topSlide,
                              showCloseIcon: true,
                              title: "Error",
                              titleTextStyle: const TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w900,
                              ),
                              desc:
                                  "The password and confirm password are not a match!",
                            ).show();
                          }
                        }
                        return null;
                      },
                      preIcon: Icons.lock,
                      preIconColor: Style.darkGrey,
                      suffIcon: _isObscureTextConfirmPassword
                          ? Icons.visibility
                          : Icons.visibility_off,
                      onPress: () {
                        setState(() {
                          _isObscureTextConfirmPassword =
                              !_isObscureTextConfirmPassword;
                        });
                      },
                      suffIconColor: Style.darkGrey),
                  SizedBox(height: 40.h),
                  getTextButton(
                    width: double.infinity,
                    height: 70.h,
                    bgColor: Style.orange,
                    fgColor: Colors.white,
                    title: "Valider",
                    fontsize: 20.sp,
                    onPressed: () {
                      if (_keyForm.currentState!.validate()) {}
                    },
                    context: context,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
