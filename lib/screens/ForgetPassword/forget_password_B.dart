// ignore_for_file: file_names
import 'package:coding_test_bmoov/components/text_field.dart';
import 'package:coding_test_bmoov/screens/ForgetPassword/forget_password_C.dart';
import 'package:coding_test_bmoov/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../components/elevated_btn.dart';

// ignore: must_be_immutable
class ForgetPasswordB extends StatefulWidget {
  String email;
  ForgetPasswordB({required this.email, super.key});

  @override
  State<ForgetPasswordB> createState() => _ForgetPasswordBState();
}

class _ForgetPasswordBState extends State<ForgetPasswordB> {
  //var
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  //controller
  TextEditingController code = TextEditingController();

  //actions

  @override
  void initState() {
    super.initState();
    var emailOriginal = widget.email.split("@");

    if (emailOriginal[0].length > 4) {
      var twoFirstLetters = emailOriginal[0].substring(0, 2);
      var emailWithStars = '*' * (emailOriginal[0].length - 4);
      var lastTwoLetters =
          emailOriginal[0].substring(emailOriginal[0].length - 2);

      var maskedEmail =
          '$twoFirstLetters$emailWithStars$lastTwoLetters@${emailOriginal[1]}';
      widget.email = maskedEmail;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Style.bgColor,
        appBar: AppBar(
            systemOverlayStyle: const SystemUiOverlayStyle(
              statusBarColor: Style.bgColor,
            ),
            centerTitle: true,
            leading: const BackButton(color: Style.orange),
            title: Text("Forgot password",
                style: TextStyle(color: Style.orange, fontSize: 15.sp))),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Form(
              key: _keyForm,
              child: Column(
                children: [
                  SizedBox(height: 80.h),
                  Text(widget.email,
                      style: TextStyle(fontSize: 13.sp),
                      textAlign: TextAlign.center),
                  SizedBox(height: 50.h),
                  getTextField(
                      ot: false,
                      hint: "code",
                      controller: code,
                      keyboardtype: TextInputType.number,
                      onChange: (val) {},
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Code cannot be empty!";
                        } else {
                          if (val.length < 5 || val.length > 5) {
                            return "Code must contain 5 characters";
                          }
                        }
                        return null;
                      },
                      preIcon: Icons.confirmation_num,
                      preIconColor: Style.darkGrey),
                  SizedBox(height: 40.h),
                  getElevatedButton(
                    title: "Continue",
                    icon: Icons.arrow_forward_ios,
                    fontsize: 20.sp,
                    bgColor: Style.orange,
                    fgColor: Colors.white,
                    onPress: () {
                      if (_keyForm.currentState!.validate()) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const ForgetPasswordC()));
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
