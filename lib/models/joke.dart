class Joke {
  int? id;
  String? type;
  String? setup;
  String? punchline;

  Joke(this.id, this.type, this.setup, this.punchline);
}
