import 'dart:ui';
import 'package:flutter/material.dart';

class Style {
  /// Colors palette
  static const Color orange = Color.fromARGB(255, 255, 117, 67); //#ff7643
  static const Color red = Color.fromARGB(255, 244, 67, 54); // #f44336
  static const Color yellow = Color.fromRGBO(255, 236, 179, 1); // ##ffecb3
  static const Color darkYellow = Color.fromARGB(255, 255, 214, 64); // ##ffd740
  static const Color bgColor = Color.fromARGB(255, 250, 250, 250); // ##fafafa
  static const Color lightGrey = Color.fromARGB(255, 242, 243, 245); // #f2f3f5
  static const Color darkGrey = Color.fromARGB(255, 103, 103, 103); // ##676767
  static const Color grey = Color.fromARGB(255, 133, 133, 135); // #9A9A9A
}
